# MyProjectAssignment

**Context**

The goal of this assignment is to :
1. Create a free GitLab account.
2. Create the following structure of the project.

├── dev
│ ├── app1
│ │ └── app.py
│ └── app2
│ └── app.py
├── prod
│ ├── app1
│ │ └── app.py
│ └── app2
│ └── app.py
└── stage
├── app1
│ └── app.py
└── app2
└── app.py


3. Create a pipeline that includes one job for each app. Each job should
be triggered and run the appropriate app.py script only if the specific
app.py file was changed when a commit is pushed to a branch.

**Solution**

The Above Project structure can be found under :

https://gitlab.com/lpassignment/myprojectassignment

There are in total 3 sub directoties , namely dev, stage and prod which denotes environments.
Each directory contains two apps namely app1 and app2 and each app has its own app.py file.

The app.py files consists of a print command. 

The CICD configuration has been kept in another project so that the template can be reused among other projects as well and can act as a centralized template. 
You can find the CICD configuration file .gitlab-ci.yml under the main branch of below project:

https://gitlab.com/lpassignment/cicd-configuration/-/blob/main/.gitlab-ci.yml?ref_type=heads

The CICD configuration file conatins six separate jobs for task 3 and the jobs get executed only when app.py file is updated of a particular app of a specific environment.

In order to execute and test the solution, Just update any app.py file from the project and check the pipeline result under :

https://gitlab.com/lpassignment/myprojectassignment/-/pipelines


**Task 5**
On a separate branch implement your solution using a downstream
pipeline (parent-child) to dynamically create a pipeline for each
environment instead of having hardcoded jobs.
Use case: you need to add additional 100 apps, so you will not add
100 new jobs for each environment because you might have only one
job per environment, which will generate jobs dynamically, e.g. based
on the project structure.

**Solution**

Under CICD Configuration project another branch is created namely Downstream which contains the CICD Configuration file for creating a downstream pipeline. This Pipeline can trigger Jobs in the main project which contains all the apps and the app.py files. Two parameters namely appname and environment are required to execute the pipeline which triggers a downstream pipeline.

The CICD configuration is present under :

https://gitlab.com/lpassignment/cicd-configuration/-/blob/Downstream/.gitlab-ci.yml?ref_type=heads

In order to run the pipeline, Just click on Run Pipeline button under :

https://gitlab.com/lpassignment/cicd-configuration/-/pipelines
Add the appname and the environment for which you want to run the job.



